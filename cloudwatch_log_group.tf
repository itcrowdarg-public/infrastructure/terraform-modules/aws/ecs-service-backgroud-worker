resource "aws_cloudwatch_log_group" "worker" {
  name              = "${var.name}-${var.environment}"
  retention_in_days = 30
}
