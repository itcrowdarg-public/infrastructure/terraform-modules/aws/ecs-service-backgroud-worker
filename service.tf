# Gets the CURRENT task definition from AWS, reflecting anything that's been deployed
# outside of Terraform (ie. CI builds).
# source: https://github.com/hashicorp/terraform-provider-aws/issues/632#issuecomment-341127191
data "aws_ecs_task_definition" "worker" {
  task_definition = "${aws_ecs_task_definition.worker.family}"
}

resource "aws_ecs_service" "worker" {
  name            = "${var.name}-${var.environment}"
  cluster         = var.environment
  task_definition = "${aws_ecs_task_definition.worker.family}:${max("${aws_ecs_task_definition.worker.revision}", "${data.aws_ecs_task_definition.worker.revision}")}"
  desired_count   = var.module_env[var.environment].service.desired_count


  deployment_maximum_percent         = var.module_env[var.environment].service.deployment_maximum_percent
  deployment_minimum_healthy_percent = var.module_env[var.environment].service.deployment_minimum_healthy_percent

  ordered_placement_strategy {
    type  = "spread"
    field = "host"
  }
}
