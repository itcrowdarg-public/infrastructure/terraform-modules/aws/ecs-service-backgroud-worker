variable "region" {
  description = "AWS region"
  type        = string
}

variable "name" {
  description = "The name of the worker"
}

variable "vpc" {
  description = "VPC where the ASG is created"
}

variable "environment" {
  description = "The name of the environment"
  type        = string
}

variable "module_env" {
  description = "The environment variables for the service"
  type        = any
}
