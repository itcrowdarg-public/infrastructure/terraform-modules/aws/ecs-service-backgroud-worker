
resource "aws_ecs_task_definition" "worker" {
  family = "${var.name}-${var.environment}"

  container_definitions = jsonencode([
    {
      name              = "${var.name}-${var.environment}"
      image             = var.module_env[var.environment].worker.image
      cpu               = var.module_env[var.environment].worker.cpu
      memoryReservation = var.module_env[var.environment].worker.memory
      essential         = true
      environment       = var.module_env[var.environment].worker.env_vars
      command           = var.module_env[var.environment].worker.command
      logConfiguration = {
        "logDriver" : "awslogs",
        "options" : {
          "awslogs-group" : aws_cloudwatch_log_group.worker.name,
          "awslogs-region" : var.region,
          "awslogs-stream-prefix" : "ecs",
        },
      }
    }
  ])
}
