# ECS Service - Background worker

## Usage

```terraform
module "ecs_service_sidekiq_worker" {
  for_each = { for i, v in var.environments : i => v }
  source   = "git::https://gitlab.com/itcrowdarg-public/infrastructure/terraform-modules/aws/ecs-service-backgroud-worker.git?ref=1.0"

  name = "worker-time"

  environment = each.value.name

  module_env = {
    production = {
      service = {
        desired_count                      = 1
        deployment_maximum_percent         = 100
        deployment_minimum_healthy_percent = 0
      }
      worker = {
        image   = "itcrowdarg/rails-multibase:mysql"
        cpu     = 256
        memory  = 256
        command = ["bundle", "exec", "sidekiq"]
        env_vars = concat(var.task_def_env_vars[each.value.name], [
          { name = "RAILS_ENV", value = each.value.name },
          { name = "MASTER_KEY", value = "THE_MASTER_KEY" },
          { name = "RAILS_LOG_TO_STDOUT", value = "1" },
          { name = "REDIS_URL", value = "redis://${module.ecs_service_redis[each.key].service_url}" },
          { name = "DATABASE_HOST", value = module.rds_cluster[each.key].cluster_endpoint },
          { name = "DATABASE_NAME", value = module.rds_cluster[each.key].cluster_database_name },
          { name = "DATABASE_USER", value = module.rds_cluster[each.key].cluster_master_username },
          { name = "DATABASE_PASS", value = module.rds_cluster[each.key].cluster_master_password },
        ])
      }

    }
    staging = {
      service = {
        desired_count                      = 1
        deployment_maximum_percent         = 100
        deployment_minimum_healthy_percent = 0
      }
      worker = {
        image   = "itcrowdarg/rails-multibase:mysql"
        cpu     = 256
        memory  = 256
        command = ["bundle", "exec", "sidekiq"]
        env_vars = concat(var.task_def_env_vars[each.value.name], [
          { name = "RAILS_ENV", value = each.value.name },
          { name = "MASTER_KEY", value = "THE_MASTER_KEY" },
          { name = "RAILS_LOG_TO_STDOUT", value = "1" },
          { name = "REDIS_URL", value = "redis://${module.ecs_service_redis[each.key].service_url}" },
          { name = "DATABASE_HOST", value = module.rds_cluster[each.key].cluster_endpoint },
          { name = "DATABASE_NAME", value = module.rds_cluster[each.key].cluster_database_name },
          { name = "DATABASE_USER", value = module.rds_cluster[each.key].cluster_master_username },
          { name = "DATABASE_PASS", value = module.rds_cluster[each.key].cluster_master_password },
        ])
      }

    }
  }

  vpc    = module.vpc
  region = var.region
}
```